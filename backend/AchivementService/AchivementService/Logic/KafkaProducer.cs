﻿using AchivementService.Models;
using Confluent.Kafka;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AchivementService.Logic
{
    public class KafkaProducer
    {
        private readonly ProducerConfig producerConfig = new ProducerConfig
        {
            //BootstrapServers = "broker:9092"
            BootstrapServers = "kafka:9092"
        };



        public void AddStep(Guid userId, Achievement achievemnt)
        {
            using (var producer = new ProducerBuilder<Null, string>(producerConfig).Build())
            {
                string topic = "UserAchievement";
                try
                {
                    //prob. should not serialize object and send as message?
                    var deliveryResult = producer.ProduceAsync(topic, new Message<Null, string> { Value= "test" })
                        .GetAwaiter()
                        .GetResult();
                }
                catch (Exception e)
                {
                    Console.WriteLine($"Oops, something went wrong: {e}");
                }

            }
        }

    }
}

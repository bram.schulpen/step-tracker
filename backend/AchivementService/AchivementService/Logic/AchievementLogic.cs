using AchivementService.Context;
using AchivementService.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AchivementService.Logic
{
    public class AchievementLogic
    {
        private readonly KafkaProducer kafkaProducer = new KafkaProducer();
        private readonly AchievementContext context;
        private Dictionary<string, int> achievementsDictionary = new Dictionary<string, int>();
        private readonly List<Achievement> achievements;

        public AchievementLogic() {
            context = new AchievementContext();
            achievements = context.GetAchievements();
            foreach (Achievement achievement in achievements) {
                achievementsDictionary.Add(achievement.Name,achievement.RequiredSteps);
            }
        }

        public void CheckAchievement(Guid userId, int steps) {
            List<Achievement> toCheckAchievement = ToCheckAchievement(userId);

            foreach (Achievement achievement in toCheckAchievement) {
                if (steps >= achievement.RequiredSteps) {
                    UserAchievement userAchievement = new UserAchievement() {
                        UserId = userId,
                        AchievementId = achievement.Id
                    };

                    if (!context.userHasAchivement(userAchievement))
                    {
                        context.AddUserAchievement(userAchievement);
                    }
                }
            }
        }

        private List<Achievement> ToCheckAchievement(Guid userId) {
            List<Achievement> toCheckAchievement = new List<Achievement>();
            List<UserAchievement> userAchievements = context.GetUserAchievements(userId);
            List<Guid> userAchivementsIds = userAchievements.Select(achivement => achivement.Id).ToList();
            foreach (Achievement achievement in achievements)
            {
                if (!userAchivementsIds.Contains(achievement.Id))
                {
                    toCheckAchievement.Add(achievement);
                }
            }
            return toCheckAchievement;
        }

        public List<Achievement> GetUserAchivements(Guid id) {
            List<UserAchievement> userAchievements = context.GetUserAchievements(id);
            List<Achievement> achievements = new List<Achievement>();

            foreach (UserAchievement userAchievement in userAchievements) {
                Achievement achievement = context.GetAchievement(userAchievement.AchievementId);
                achievements.Add(achievement);
            }
            return achievements;
        }

        public List<Achievement> test(Guid id) {
            List<UserAchievement> userAchievements = context.GetUserAchievements(id);
            List<Achievement> achievements = new List<Achievement>();

            foreach (UserAchievement userAchievement in userAchievements) {
                Achievement achievement = context.GetAchievement(userAchievement.AchievementId);
                achievements.Add(achievement);
            }
            return achievements;
        }

        public void AddAchievement(Achievement achivement){
                context.AddAchievement(achivement);         
        }
    }
}

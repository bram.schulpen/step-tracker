﻿
using AchivementService.Dtos;
using AchivementService.Logic;
using Confluent.Kafka;
using Confluent.Kafka.Admin;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using System;
using System.IO;
using System.Threading;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace ST_KafkaConsumer.Handlers
{
    public class KafkaConsumerHandler : BackgroundService
    {
        private readonly string topic = "step";
        private static AchievementLogic _achivementLogic;
        private static IConfiguration _configuration;
      //  private readonly string server = "localhost:9092";
        // private readonly string server = "broker:9092";
        private readonly string server = "kafka:9092";

        public KafkaConsumerHandler(IConfiguration config)
        {
            _configuration = config;
        }

        private static void InitializeLogic()
        {
            _achivementLogic = new AchievementLogic();
        }

        private void CreateTopic()
        {
            using (var adminClient = new AdminClientBuilder(new AdminClientConfig { BootstrapServers = server }).Build())
            {
                try
                {
                    adminClient.CreateTopicsAsync
                        (
                        new TopicSpecification[] {

                           new TopicSpecification { Name = "step", ReplicationFactor = 1, NumPartitions = 1 },
            });
                }
                catch (CreateTopicsException e)
                {
                    Console.WriteLine($"An error occured creating topic {e.Results[0].Topic}: {e.Results[0].Error.Reason}");
                }
            }
        }


        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            InitializeLogic();
            CreateTopic();
            var config = new ConsumerConfig()
            {
                // GroupId = "st_consumer_group",
                GroupId = "st_consumer_group_achivement",
                BootstrapServers = server,
                AutoOffsetReset = AutoOffsetReset.Earliest,
                AllowAutoCreateTopics = true
            };

            await Task.Run(() =>
            {
                using (var consumer = new ConsumerBuilder<Ignore, string>(config).Build())
                {
                    config.AllowAutoCreateTopics = true;
                    consumer.Subscribe(topic);

                    while (!stoppingToken.IsCancellationRequested)
                    {
                        var consumeResult = consumer.Consume(stoppingToken);
                        StepPointDto step = GetStep(consumeResult.Message.Value);

                        _achivementLogic.CheckAchievement(step.UserId, step.Amount);
                    }
                    consumer.Close();
                }
            });
        }


        private StepPointDto GetStep(string message)
        {
            var serializer = new XmlSerializer(typeof(StepPointDto));
            StepPointDto step;
            using (TextReader reader = new StringReader(message))
            {
                step = (StepPointDto)serializer.Deserialize(reader);
            }
            return step;
        }


    }
}

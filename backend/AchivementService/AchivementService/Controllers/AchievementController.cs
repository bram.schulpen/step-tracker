﻿using AchivementService.Logic;
using AchivementService.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Net.Http.Headers;
using ST_KafkaConsumer.Handlers;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Threading.Tasks;

namespace AchivementService.Controllers
{

    [Route("api/v1")]
    [ApiController]
    public class AchievementController : ControllerBase
    {
        AchievementLogic achivementLogic = new AchievementLogic();

        public AchievementController() {
        //    KafkaConsumerHandler handler = new KafkaConsumerHandler();

        }

        [HttpPost]
        [Route("/addAchievement")]
        public IActionResult AddAchievement([FromBody] Achievement achievement ) {
            achivementLogic.AddAchievement(achievement);
            return Ok();
        }

        [HttpGet]
        [Route("/getAchievements/{userId}")]
        public IActionResult GetAchievement(string userId)
        {
            List<Achievement> achievements =  achivementLogic.GetUserAchivements(Guid.Parse(userId));
            return new JsonResult(achievements);
        }

        [HttpGet]
        [Route("/test/{amount}")]
        public IActionResult test(int amount)
        {
            string accessToken = Request.Headers[HeaderNames.Authorization].ToString().Replace("Bearer ", string.Empty);
            var handler = new JwtSecurityTokenHandler();
            var jsonToken = handler.ReadJwtToken(accessToken);
            string userId = jsonToken.Claims.First(i => i.Type == "sid").Value;
            achivementLogic.CheckAchievement(Guid.Parse(userId),amount);
            return Ok();
        }

        [HttpGet]
        [Route("/test")]
        public IActionResult test()
        {
            return Ok();
        }
    }
}

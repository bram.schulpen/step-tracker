﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AchivementService.Logic;
using AchivementService.Models;
using MongoDB.Bson;
using MongoDB.Bson.Serialization;
using MongoDB.Bson.Serialization.Serializers;
using MongoDB.Driver;

namespace AchivementService.Context
{
    public class AchievementContext
    {
        private IMongoDatabase db;
        private string db_name = "achievementDB";
        private string achievement_collection = "achievements";
        private string user_achievement_collection = "user_achievements";

        public AchievementContext() {
            //   MongoClient dbClient = new MongoClient();
            MongoClient dbClient = new MongoClient("mongodb+srv://bram:fIknVRs3yPsiZDmq@cluster0mongo.ftdqk.mongodb.net/" + db_name + "?retryWrites=true&w=majority");
            db = dbClient.GetDatabase(db_name);
        }


        public List<Achievement> GetAchievements()
        {
            IMongoCollection<Achievement> collection = db.GetCollection<Achievement>(achievement_collection);
            List<Achievement> achievements = collection.Find(new BsonDocument()).ToList();
            return achievements;
        }


        public Achievement GetAchievement(Guid id)
        {
            IMongoCollection<Achievement> collection = db.GetCollection<Achievement>(achievement_collection);
            var filter = Builders<Achievement>.Filter.Eq("Id", id.ToString());
            Achievement achievements = collection.Find(filter).FirstOrDefault(); ;
            return achievements;
        }

        public List<UserAchievement> GetUserAchievements(Guid userId)
        {
            IMongoCollection<UserAchievement> collection = db.GetCollection<UserAchievement>(user_achievement_collection);
            var filter = Builders<UserAchievement>.Filter.Eq("UserId", userId.ToString());
            List<UserAchievement> achievements = collection.Find(filter).ToList();
            return achievements;
        }

        public void AddAchievement(Achievement achievement)
        {
            IMongoCollection<Achievement> collection = db.GetCollection<Achievement>(achievement_collection);
            collection.InsertOne(achievement);
        }


        public bool userHasAchivement(UserAchievement achievement) {
            IMongoCollection<UserAchievement> collection = db.GetCollection<UserAchievement>(user_achievement_collection);
            var filter = Builders<UserAchievement>.Filter.Eq("UserId", achievement.UserId) & Builders<UserAchievement>.Filter.Eq("AchievementId", achievement.AchievementId);
            bool exists = collection.Find(filter).Any();
            return exists;
        }

        public void AddUserAchievement<UserAchievement>(UserAchievement achievement)
        {
            IMongoCollection<UserAchievement> collection = db.GetCollection<UserAchievement>(user_achievement_collection);
            collection.InsertOne(achievement);
        }


    }
}

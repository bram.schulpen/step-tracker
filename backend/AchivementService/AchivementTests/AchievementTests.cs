using AchivementService.Context;
using AchivementService.Logic;
using AchivementService.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MongoDB.Driver;
using System;
using System.Collections.Generic;

namespace AchivementTests
{
    [TestClass]
    public class AchievementTests
    {
        private string db_name = "achievementDB";
        private string achievement_collection = "achievements";
        private string user_achievement_collection = "user_achievements";
        private IMongoDatabase db;
        private Guid userId = new Guid(); 


        [TestInitialize()]
        public void TestInitialize() {
            CleanCollection();
        }

        [TestCleanupAttribute()]
        public void AssemblyCleanup() {
            CleanCollection();
        }

        private void CleanCollection() {
            MongoClient dbClient = new MongoClient();
            db = dbClient.GetDatabase(db_name);
            db.DropCollection(achievement_collection);
            db.DropCollection(user_achievement_collection);
            List<Achievement> achivements = new List<Achievement>() {
                new Achievement() { Name = "First steps", RequiredSteps = 10000 },
                new Achievement() { Name = "Test2", RequiredSteps = 100000 },
                new Achievement() { Name = "Hero", RequiredSteps = 1000000 }
            };

            foreach (Achievement achievement in achivements)
            {
                IMongoCollection<Achievement> collection = db.GetCollection<Achievement>(achievement_collection);
                collection.InsertOne(achievement);
            }
        }

        [TestMethod]
        public void AddAchievementInsertedTrue()
        {
            //arrange 
            AchievementLogic achievementLogic = new AchievementLogic();
            AchievementContext achievementContext = new AchievementContext();
            Achievement achievement = new Achievement() { Name = "test", RequiredSteps= 1 };
            List<Achievement> db_achivements = new List<Achievement>();

            //act
            achievementLogic.AddAchievement(achievement);
            db_achivements = achievementContext.GetAchievements();

            //assert;
            Assert.AreEqual(db_achivements[3].Name, achievement.Name);
        }


        [TestMethod]
        public void UserAddsStepsGetAchievementFalse()
        {
            //arrange 
            AchievementLogic achievementLogic = new AchievementLogic();
            int steps = 900;

            //act
            achievementLogic.CheckAchievement(userId, steps);
            List<Achievement> user_achivements = achievementLogic.GetUserAchivements(userId);

            //assert
            Assert.IsTrue(user_achivements.Count == 0 );
        }



        [TestMethod]
        public void UserAddsStepsGetAchievementTrue()
        {
            //arrange 
            AchievementLogic achievementLogic = new AchievementLogic();
            int steps = 10000;

            //act
            achievementLogic.CheckAchievement(userId, steps);
            List<Achievement> user_achivements = achievementLogic.GetUserAchivements(userId);

            //assert
            Assert.AreEqual(user_achivements[0].Name, "First steps");
        }
    }
}

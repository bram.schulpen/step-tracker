﻿using ApiGatewayTracker.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Net.Http.Headers;
using StepService.Context;
using StepService.Handlers;
using StepService.Logic;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace StepService.Controllers
{

    [ApiController]
    [Route("api/kafka")]
    public class StepController : ControllerBase
    {
        private IConfiguration _config;


        private readonly StepLogic logic;
        public StepController(IConfiguration config)
        {
            _config = config;
            logic = new StepLogic();
        }

        /*
        [HttpGet]
        [Route("/api/step_report")]
        public IActionResult getSteps()
        {
            string accessToken = Request.Headers[HeaderNames.Authorization].ToString().Replace("Bearer ", string.Empty);
            string userId = getSenderId(accessToken);
            List<StepPoint> points = logic.getUserSteps(Guid.Parse(userId));
            logic.GenerateStepOverView(points, Guid.Parse(userId));    
            string pdfFilePath = "Resources/Reports/Nr" + userId + ".pdf";
            var stream = new FileStream(pdfFilePath, FileMode.Open);
            return new FileStreamResult(stream, "application/pdf");

        }

        [HttpGet]
        [Route("/test")]
        public IActionResult test()
        {
            return Ok();

        }
        */

        [HttpPost]
        [Route("/api/add_step")]
        public IActionResult addStep([FromBody] int stepAmount)
        {
            try
            {
                string accessToken = Request.Headers[HeaderNames.Authorization].ToString().Replace("Bearer ", string.Empty);
                var handler = new JwtSecurityTokenHandler();
                var jsonToken = handler.ReadJwtToken(accessToken);
                string userId = jsonToken.Claims.First(i => i.Type == "sid").Value;
                logic.AddStep(Guid.Parse(userId), stepAmount);
                return new OkResult();
            }
            catch (Exception)
            {
                return new BadRequestResult();
            }
        }

        [HttpGet]
        [Route("/api/step_amount/{id}")]
        public IActionResult getSteps(Guid id)
        {
            List<StepPoint> response = logic.getUserSteps(id);
            return new JsonResult(response);
        }

        [HttpGet]
        [Route("/api/step_amount_other/{id}")]
        public IActionResult getStepsOther(Guid id)
        {
            List<StepPoint> response = logic.getOtherUserSteps(id);
            return new JsonResult(response);
        }

        private string getSenderId(string accessToken)
        {
            var handler = new JwtSecurityTokenHandler();
            var jsonToken = handler.ReadJwtToken(accessToken);
            string userId = jsonToken.Claims.First(i => i.Type == "sid").Value;
            return userId;
        }
    }
}

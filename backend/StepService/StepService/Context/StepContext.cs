﻿using ApiGatewayTracker.Models;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace StepService.Context
{
    public class StepContext
    {
        private IMongoDatabase db;
        private string db_name = "stepHistoryDB";
        private string steps_collection = "steps";


        public StepContext()
        {
            // MongoClient dbClient = new MongoClient();
            MongoClient dbClient = new MongoClient("mongodb+srv://bram:fIknVRs3yPsiZDmq@cluster0mongo.ftdqk.mongodb.net/" + db_name + "?retryWrites=true&w=majority");
            db = dbClient.GetDatabase(db_name);
        }

        private bool FirstDaySteps(StepPoint step, IMongoCollection<StepPoint> collection,DateTime date)
        {
            var filter = Builders<StepPoint>.Filter.Eq("Date", date) & Builders<StepPoint>.Filter.Eq("UserId", step.UserId);
            var exists = collection.Find(filter).Any();
            return !exists;
        }


        public void AddStep<T>(StepPoint step)
        {
            DateTime date = DateTime.Now.Date;
            IMongoCollection<StepPoint> collection = db.GetCollection<StepPoint>(steps_collection);
            var filter = Builders<StepPoint>.Filter.Eq("Date", date) & Builders<StepPoint>.Filter.Eq("UserId", step.UserId);
            if (FirstDaySteps(step,collection, date))
            {
                System.Console.WriteLine("First dat");
                InsertRecord("steps", step);
            }
            else {
                System.Console.WriteLine("second day");
                StepPoint oldPoint = collection.Find(filter).FirstOrDefault();
                oldPoint.Amount = step.Amount;
                collection.ReplaceOne(filter, oldPoint);
            }
        }


        private void InsertRecord<T>(string table, T record)
        {
            IMongoCollection<T> collection = db.GetCollection<T>(table);
            collection.InsertOne(record);
        }



        public List<StepPoint> GetUserSteps(Guid userId) {
            IMongoCollection<StepPoint> collection = db.GetCollection<StepPoint>(steps_collection);
            var filter = Builders<StepPoint>.Filter.Eq("UserId", userId.ToString());
            List<StepPoint> userSteps = collection.Find(filter).Limit(365).SortByDescending(userStep => userStep.Date).ToList();
            return userSteps;
        }

        public List<StepPoint> GetOtherUserSteps(Guid userId)
        {
            IMongoCollection<StepPoint> collection = db.GetCollection<StepPoint>(steps_collection);
            var filter = Builders<StepPoint>.Filter.Eq("UserId", userId.ToString());
            List<StepPoint> userSteps = collection.Find(filter).SortByDescending(userStep => userStep.Date)
                .Limit(1).ToList();
            return userSteps;
        }



    }
}

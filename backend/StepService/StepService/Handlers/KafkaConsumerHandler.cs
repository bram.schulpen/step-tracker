﻿using ApiGatewayTracker.Models;
using Confluent.Kafka;
using Confluent.Kafka.Admin;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using StepService.Context;
using StepService.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace StepService.Handlers
{
    public class KafkaConsumerHandler: BackgroundService
    {
        private readonly string topic = "step";
        private static StepContext _context;
        private static IConfiguration _configuration;
        private readonly string server = "kafka:9092";
        // private readonly string server = "broker:9092";
      //  private readonly string server = "localhost:9092";

        public KafkaConsumerHandler(IConfiguration config)
        {
            _configuration = config;
        }

        private static void InitializeContex()
        {
            _context = new StepContext();
        }

        private void CreateTopic()
        {
            using (var adminClient = new AdminClientBuilder(new AdminClientConfig { BootstrapServers = server }).Build())
            {
                try
                {
                    adminClient.CreateTopicsAsync
                        (
                        new TopicSpecification[] {

                           new TopicSpecification { Name = "step", ReplicationFactor = 1, NumPartitions = 1 },
            });
                }
                catch (CreateTopicsException e)
                {
                    Console.WriteLine($"An error occured creating topic {e.Results[0].Topic}: {e.Results[0].Error.Reason}");
                }
            }
        }


        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            InitializeContex();
            CreateTopic();
            var config = new ConsumerConfig
            {
                GroupId = "step_group",
                BootstrapServers = server,
                AutoOffsetReset = AutoOffsetReset.Earliest,
                AllowAutoCreateTopics = true
            };

            await Task.Run(() =>
            {
                using (var consumer = new ConsumerBuilder<Ignore, string>(config).Build())
                {
                    config.AllowAutoCreateTopics = true;
                    consumer.Subscribe(topic);

                    while (!stoppingToken.IsCancellationRequested)
                    {
                        var consumeResult = consumer.Consume(stoppingToken);

                        var serializer = new XmlSerializer(typeof(StepPoint));
                        using (TextReader reader = new StringReader(consumeResult.Message.Value))
                        {
                            StepPointDto stepDto = GetStep(consumeResult.Message.Value);
                            StepPoint step = new StepPoint()
                            {
                                Amount = stepDto.Amount,
                                Date = stepDto.Date,
                                UserId = stepDto.UserId,
                                Id = stepDto.Id
                            };
                            _context.AddStep<StepPointDto>(step);
                            System.Console.WriteLine("getting step");
                        }
     
                    }
                    consumer.Close();
                }
            });
        }

        private StepPointDto GetStep(string message)
        {
            var serializer = new XmlSerializer(typeof(StepPointDto));
            StepPointDto step;
            using (TextReader reader = new StringReader(message))
            {
                step = (StepPointDto)serializer.Deserialize(reader);
            }
            return step;
        }
    }
}
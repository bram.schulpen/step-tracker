﻿using ApiGatewayTracker.Models;
using iText.IO.Font.Constants;
using iText.IO.Image;
using iText.Kernel.Colors;
using iText.Kernel.Font;
using iText.Kernel.Pdf.Canvas.Draw;
using iText.Layout;
using iText.Layout.Element;
using iText.Layout.Properties;
using StepService.Context;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace StepService.Logic
{
    public class StepLogic
    {
        StepContext stepContext;


        public void AddStep(Guid userId, int steps) {
            StepPoint stepPoint = new StepPoint
            {
                UserId = userId,
                Amount = steps,
                Date = DateTime.Now.Date
            };
            stepContext.AddStep<StepPoint>(stepPoint);
        }

        public StepLogic() {
            stepContext = new StepContext();
        }

        public void GenerateStepOverView(List<StepPoint> stepPoints, Guid id)
        {
            string[] columnNames = { "Date", "Steps amount" };
            string[] headerNames = { "StepTracker Inc", "Sittard-geleen/Hansstraat 170", "1249HD Limburg/Sittard", "Netherlands" };
            string[] PersonHeaderNames = { "Step Report" };
            int[] marginsStep = { -80, 0, 0, 0 };
            int[] marginsPerson = { -65, 0, 0, 0 };
            SolidLine line = new SolidLine(1f);
            iText.Layout.Element.LineSeparator ls = new LineSeparator(line);
            iText.Kernel.Font.PdfFont boldFont = PdfFontFactory.CreateFont(StandardFonts.HELVETICA_BOLD);
            using (iText.Kernel.Pdf.PdfWriter pdfWriter = new iText.Kernel.Pdf.PdfWriter("Resources/Reports/Nr" + id + ".pdf"))
            using (iText.Kernel.Pdf.PdfDocument pdfDocument = new iText.Kernel.Pdf.PdfDocument(pdfWriter))
            using (Document document = new Document(pdfDocument))
            {

                //add the images
                ImageData data = ImageDataFactory.Create("Resources/Images/Logo_resized.PNG");
                Image companyLogo = new Image(data);
                document.Add(companyLogo);


                for (int i = 0; i < headerNames.Length; i++)
                {
                    CreateHeadings(document, headerNames[i], marginsStep[i], 150);
                }

                for (int i = 0; i < PersonHeaderNames.Length; i++)
                {
                    CreateHeadings(document, PersonHeaderNames[i], marginsPerson[i], 400);
                }


                Table table = new Table(new float[columnNames.Length]).UseAllAvailableWidth();
                table.SetMarginTop(70);
                table.SetMarginBottom(0);

                // first row
                foreach (string name in columnNames)
                {
                    table.AddCell(name).SetFontSize(10);
                }

                foreach (StepPoint item in stepPoints)
                {
                    table.AddCell(item.Date.ToString()).SetFontSize(10);
                    table.AddCell(item.Amount.ToString()).SetFontSize(10);

                }
                document.Add(table);

            }
        }
        private void CreateHeadings(Document document, string text, int marginTop, int marginLeft)
        {
            Paragraph header = new Paragraph(text)
                .SetFont(PdfFontFactory.CreateFont(StandardFonts.HELVETICA))
                .SetMarginLeft(marginLeft)
                .SetMarginTop(marginTop)
                .SetFontSize(8)
                .SetFontColor(ColorConstants.BLACK);
            document.Add(header);
        }


        private void AddHeader(Document document)
        {
            Paragraph header = new Paragraph("Rekening rijden belansting")
                .SetFont(PdfFontFactory.CreateFont(StandardFonts.HELVETICA))
                .SetFontSize(14)
                .SetFontColor(ColorConstants.BLACK)
                .SetTextAlignment(TextAlignment.RIGHT);
            document.Add(header);
        }

   

        public List<StepPoint> getUserSteps(Guid userId) {
            List<StepPoint> userSteps = new List<StepPoint>();
            userSteps = stepContext.GetUserSteps(userId);
            return userSteps;
        }

        public List<StepPoint> getOtherUserSteps(Guid userId)
        {
            List<StepPoint> userSteps = new List<StepPoint>();
            userSteps = stepContext.GetOtherUserSteps(userId);
            return userSteps;
        }


    }
}

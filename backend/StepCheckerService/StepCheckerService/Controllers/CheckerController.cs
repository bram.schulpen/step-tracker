﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Microsoft.Net.Http.Headers;
using Prometheus;
using StepCheckerService.Logic;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Threading.Tasks;

namespace StepCheckerService.Controllers
{

    [ApiController]
    [Route("api/step_checker")]
    public class CheckerController : ControllerBase
    {
        private Counter ProcessedJobCount = Metrics.CreateCounter("dotnet_request_operations_total", "The total number of processed requests");
        private Counter CallNumber = Metrics.CreateCounter("step_checker_service_requests_total", "The total number of calls to the step checker server");

        private readonly ILogger<CheckerController> _logger;
        private CheckerLogic checkerLogic;

        public CheckerController(ILogger<CheckerController> logger)
        {
            _logger = logger;
            checkerLogic = new CheckerLogic();

        }

        [HttpPost]
       // [Authorize]
        public IActionResult AddStep([FromBody] int stepAmount)
        {
            CallNumber.Inc();
            try
            {
                
                string accessToken = Request.Headers[HeaderNames.Authorization].ToString().Replace("Bearer ", string.Empty);
                var handler = new JwtSecurityTokenHandler();
                var jsonToken = handler.ReadJwtToken(accessToken);
                string userId = jsonToken.Claims.First(i => i.Type == "sid").Value;
                checkerLogic.AddStep(Guid.Parse(userId), stepAmount);
                return new OkResult();
            }
            catch (Exception)
            {
                return new BadRequestResult();
            }
        }


        [HttpGet]
        [Route("/test")]
        public IActionResult test2()
        {
            return Ok();
        }
    

    [HttpGet]
        public IActionResult test()
        {
            CallNumber.Inc();
            return Ok();
            
            var sw = Stopwatch.StartNew();
            sw.Stop();
            ProcessedJobCount.Inc();
            var histogram =
            Metrics.CreateHistogram(
                    "dotnet_request_duration_seconds",
                    "Histogram for the duration in seconds.",
                    new HistogramConfiguration
                    {
                        Buckets = Histogram.LinearBuckets(start: 1, width: 1, count: 5)
                    });

            histogram.Observe(sw.Elapsed.TotalSeconds);
            return Ok();          
        }
    }
}


﻿using Confluent.Kafka;
using StepCheckerService.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace StepCheckerService.Logic
{
    public class CheckerLogic
    {
        private readonly ProducerConfig producerConfig = new ProducerConfig
        {
         //   BootstrapServers = "localhost:9092"
           // BootstrapServers = "broker:9092"
              BootstrapServers = "kafka:9092"
        };



        public void AddStep(Guid userId, int steps)
        {
            using (var producer = new ProducerBuilder<Null, string>(producerConfig).Build())
            {
                string topic = "step";
                try
                {
                    //string message = "test";
                     string message = SerializeStep(userId, steps);
                    //  producer.Produce(topic, new Message<Null, string> { Value = message });
                    producer.Produce(topic, new Message<Null, string> { Value = message });
                        // .GetAwaiter()
                       // .GetResult();
                }
                catch (Exception e)
                {
                    Console.WriteLine($"Oops, something went wrong: {e}");
                }
            }
        }

        // this can't be a good way to send steps :/
        private string SerializeStep(Guid userId, int steps)
        {
            StepPointDto stepPoint = new StepPointDto
            {
                UserId = userId,
                Amount = steps,
                Date = DateTime.Now.Date
            };

            XmlSerializer xmlSerializer = new XmlSerializer(stepPoint.GetType());

            using (StringWriter textWriter = new StringWriter())
            {
                xmlSerializer.Serialize(textWriter, stepPoint);
                return textWriter.ToString();
            }
        }
    }
}

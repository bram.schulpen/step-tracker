﻿using RestSharp;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using UserService.Models;

namespace UserService.DataLayer
{
    public class StepLayer
    {
         private readonly string step_amount_url = "https://step.steptracker.life/api/step_amount";
          private readonly string step_other_url = "https://step.steptracker.life/api/step_amount_other";
        // private readonly string step_amount_url = "http://host.docker.internal:7095/api/step_amount";
        //  private readonly string step_other_url = "http://host.docker.internal:7095/api/step_amount_other";
        // private readonly string step_amount_url = "https://localhost:44391/api/step_amount";
        //private readonly string step_other_url = "https://localhost:44391/api/step_amount_other";

        public async Task<List<StepPoint>> GetStepsAsync(Guid id) {
            HttpClient client = new HttpClient();
            // Update port # in the following line.
            client.BaseAddress = new Uri(step_amount_url);
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(
                new MediaTypeWithQualityHeaderValue("application/json"));
            try
            {
                List<StepPoint> steps = new List<StepPoint>();
                var request = new RestRequest(Method.GET);
               HttpRequestMessage message = new HttpRequestMessage();
                  message.RequestUri = new Uri(step_amount_url + "/" + id);
                HttpResponseMessage response = await client.SendAsync(message);
                List<StepPoint> stepPoints = new List<StepPoint>();

                if (response.IsSuccessStatusCode)
                {
                    var content = response.Content;
                    string jsonContent = content.ReadAsStringAsync().Result;
                     stepPoints = Newtonsoft.Json.JsonConvert.DeserializeObject<List<StepPoint>>(jsonContent);
                }
                return stepPoints;
            }
            catch (Exception e)
            {
                return null;
            }
        }


        public async Task<List<StepPoint>> GetOtherStepsAsync(Guid id)
        {
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(step_other_url);
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(
                new MediaTypeWithQualityHeaderValue("application/json"));
            try
            {
                List<StepPoint> steps = new List<StepPoint>();
                var request = new RestRequest(Method.GET);
                HttpRequestMessage message = new HttpRequestMessage();
                message.RequestUri = new Uri(step_other_url + "/" + id);
                HttpResponseMessage response = await client.SendAsync(message);
                List<StepPoint> stepPoints = new List<StepPoint>();

                if (response.IsSuccessStatusCode)
                {
                    var content = response.Content;
                    string jsonContent = content.ReadAsStringAsync().Result;
                    stepPoints = Newtonsoft.Json.JsonConvert.DeserializeObject<List<StepPoint>>(jsonContent);
                }
                return stepPoints;
            }
            catch (Exception e)
            {
                return null;
            }
        }

    }



}

//step_amount_other
﻿using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using UserService.Models;

namespace UserService.DataLayer
{
    public class AchievementHandler
    {
        //private readonly string achievement_url = "https://localhost:44362/getAchievements";
        private readonly string achievement_url = "https://achievement.steptracker.life/getAchievements";

        public async Task<List<Achievement>> GetAchievement(string id)
        {
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(achievement_url);
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(
                new MediaTypeWithQualityHeaderValue("application/json"));
            try
            {
                List<Achievement> steps = new List<Achievement>();
                var request = new RestRequest(Method.GET);
                HttpRequestMessage message = new HttpRequestMessage();
                message.RequestUri = new Uri(achievement_url + "/" + id);
                HttpResponseMessage response = await client.SendAsync(message);
                List<Achievement> achievement = new List<Achievement>();

                if (response.IsSuccessStatusCode)
                {
                    var content = response.Content;
                    string jsonContent = content.ReadAsStringAsync().Result;
                    achievement = Newtonsoft.Json.JsonConvert.DeserializeObject<List<Achievement>>(jsonContent);
                }
                return achievement;
            }
            catch (Exception e)
            {
                return null;
            }
        }

    }
}

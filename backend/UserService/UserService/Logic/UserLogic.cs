﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using UserService.Context;
using UserService.DataLayer;
using UserService.Dtos;
using UserService.Models;

namespace UserService.Logic
{
    public class UserLogic
    {
        private readonly AchievementHandler _achievementHandler;
        private readonly StepLayer _stepLayer;
        private readonly UserContext _context;
        public UserLogic(UserContext context){
            _context = context;
            _stepLayer = new StepLayer();
            _achievementHandler = new AchievementHandler();
        }

        public List<UserDto> getAllUsers(Guid userId)
        {
            List<Followed> followeds = getFollowed(userId);
            // this should probabily not be allowed
            List<UserDto> usersDtos = new List<UserDto>();
            //TODO check this statemen!
            List<Guid> followedIds = followeds.Where(followed => followed.UserId == userId).Select(followedUser => followedUser.ToFollowId).ToList();
            List <User> users = _context.GetUsers<User>(userId);
            foreach (User user in users) {
                UserDto userDto = new UserDto()
                {
                    Id = user.Id,
                    Email = user.email,
                    Username = user.username,
                    Followed = followedIds.Contains(user.Id)
            };
                usersDtos.Add(userDto);
             }
            return usersDtos;
        }

        public async Task<List<Achievement>> GetAchievement(string userId)
        {

            List<Achievement> achievement = await _achievementHandler.GetAchievement(userId);
            return achievement;
        }

        public UserDto getUser(Guid userId, string username)
        {
                User user = _context.GetUser(username);
                if (user != null && user.Id != userId)
                {
                    bool doesFollow = _context.DoesUserFollow(userId, user.Id);
                    UserDto userDto = new UserDto()
                    {
                        Username = user.username,
                        Id = user.Id,
                        Followed = doesFollow

                    };
                    return userDto;
                }
                return null;
            
        }

        public List<Followed> getFollowed(Guid userId) {
           List<Followed> followed =  _context.GetFollowing<Followed>(userId);

            return followed;
        }

        public async Task<List<FollowedStepsDto>> GetUserSteps(string userId)
        {
            List<FollowedStepsDto> followedStepsDtos = new List<FollowedStepsDto>();
            Guid guid = Guid.Parse(userId); 
                string username = _context.GetUsersname(guid);
                List<StepPoint> points = await _stepLayer.GetStepsAsync(guid);
                foreach (StepPoint point in points)
                {
                    FollowedStepsDto followedStepsDto = new FollowedStepsDto()
                    {
                        Username = username,
                        Amount = point.Amount,
                         Date = point.Date
                    };
                followedStepsDtos.Add(followedStepsDto);
                }
            return followedStepsDtos;
        }


        public async Task<List<FollowedStepsDto>> GetFollowedStepsAsync(string userId) {
            List<Followed> followed = getFollowed(Guid.Parse(userId));
            List<FollowedStepsDto> followedStepsDtos = new List<FollowedStepsDto>();
            foreach (Followed user in followed) {
                string username = _context.GetUsersname(user.ToFollowId);
                List<StepPoint> points = await _stepLayer.GetOtherStepsAsync(user.ToFollowId);
                foreach (StepPoint point in points) {
                    FollowedStepsDto followedStepsDto = new FollowedStepsDto() {
                        Username = username,
                        Amount = point.Amount,
                        Date = point.Date
                    };
                    followedStepsDtos.Add(followedStepsDto);
                }
            }
            return followedStepsDtos;
        }

        public void followUser(Guid userId, Guid toFollowId) {
            if (_context.DoesUserFollow(userId, toFollowId) == false) {
                Followed followed = createFollowed(userId, toFollowId);
                _context.AddFollowing<Followed>(followed);
            }
        }

        public void unfollowUser(Guid userId, Guid toFollowId)
        {
            if (_context.DoesUserFollow(userId, toFollowId))
            {
                _context.RemoveFollowing(userId, toFollowId);
            }
        }

        private Followed createFollowed(Guid userId, Guid toFollowId) {
            Followed followed = new Followed()
            {
                UserId = userId,
                ToFollowId = toFollowId
            };
            return followed;
    }
    }
}

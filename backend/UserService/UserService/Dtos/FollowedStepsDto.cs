﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace UserService.Dtos
{
    public class FollowedStepsDto
    {
        public string Username { get; set; }
        public int Amount { get; set; }
        public DateTime Date { get; set; }
    }
}

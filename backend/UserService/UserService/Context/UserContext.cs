﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization;
using MongoDB.Bson.Serialization.Serializers;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using UserService.Dtos;
using UserService.Models;

namespace UserService.Context
{
    public class UserContext
    {
        private IMongoDatabase db;
        private string db_name = "followerDB";
        private string follow_collection = "user_followers";
        private string user_collection = "user_info";


        public UserContext()
        {
            //  MongoClient dbClient = new MongoClient();
            MongoClient dbClient = new MongoClient("mongodb+srv://bram:fIknVRs3yPsiZDmq@cluster0mongo.ftdqk.mongodb.net/" + db_name + "?retryWrites=true&w=majority");
            db = dbClient.GetDatabase(db_name);
        }

        //BASIC operations
        private void InsertRecord<T>(T record, string collection_name)
        {
            IMongoCollection<T> collection = db.GetCollection<T>(collection_name);
            collection.InsertOne(record);
        }

        private void RemoveRecord<T>(string collection_name, FilterDefinition<T> filter)
        {
            IMongoCollection<T> collection = db.GetCollection<T>(collection_name);
            collection.DeleteOne(filter);
        }

        //Entry following
        public void AddFollowing<T>(Followed toFollow)
        {
            InsertRecord(toFollow, follow_collection);
        }
        public List<T> GetFollowing<T>(Guid userId)
        {
            IMongoCollection<T> collection = db.GetCollection<T>(follow_collection);
            var filter = Builders<T>.Filter.Eq("UserId", userId);
            List<T> following = collection.Find(filter).ToList();
            return following;
        }

        public bool DoesUserFollow(Guid userId, Guid FollowUserId) {
            IMongoCollection<Followed> collection = db.GetCollection<Followed>(follow_collection);
            var filter = Builders<Followed>.Filter.Eq("UserId", userId.ToString()) & Builders<Followed>.Filter.Eq("ToFollowId", FollowUserId.ToString());
            var exists = collection.Find(filter).Any();
            return exists;
        }

        public void RemoveFollowing(Guid userId, Guid ToFollowId)
        {
            var filter = Builders<BsonDocument>.Filter.Eq("UserId", userId.ToString()) & Builders<BsonDocument>.Filter.Eq("ToFollowId", ToFollowId.ToString());
            RemoveRecord<BsonDocument>(follow_collection,filter);
        }

        //Entry users
        public List<User> GetUsers<T>(Guid userId)
        {
            IMongoCollection<User> collection = db.GetCollection<User>(user_collection);
             var filter = Builders<User>.Filter.Ne("Id", userId.ToString());
            List<User> users = collection.Find(filter).Limit(5).ToList();
            return users;
        }

        public User GetUser(string username)
        {
            IMongoCollection<User> collection = db.GetCollection<User>(user_collection);
            var filter = Builders<User>.Filter.Eq("username", username);
            User user = collection.Find(filter).FirstOrDefault();
           // List<User> users = collection.Find(filter).ToList();
            return user;
        }

        public string GetUsersname(Guid userId)
        {
            IMongoCollection<User> collection = db.GetCollection<User>(user_collection);
            var filter = Builders<User>.Filter.Eq("Id", userId.ToString());
            User user = collection.Find(filter).FirstOrDefault();
            return user.username;
        }

        public void addUser(User user) {
            InsertRecord(user, user_collection);
        }
        public void removeUser(User user) {
            var filter = Builders<BsonDocument>.Filter.Eq("Id", user.Id.ToString());
            RemoveRecord<BsonDocument>(user_collection, filter);
            //TODO remove from other colleciton....
                
        }


    }
}

﻿using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace UserService.Models
{
    public class Achievement
    {
        [BsonId]
        [BsonRepresentation(MongoDB.Bson.BsonType.String)]
        public Guid Id { get; set; }


        [BsonRequired]
        public string Name { get; set; }

        [BsonRequired]
        public int RequiredSteps { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace UserService.Models
{
    public class StepPoint
    {
        public Guid Id { get; set; }

        public Guid UserId { get; set; }

        public int Amount { get; set; }

        public DateTime Date { get; set; }
    }
}

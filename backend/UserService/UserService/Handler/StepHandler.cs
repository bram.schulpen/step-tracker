﻿using Confluent.Kafka;
using Confluent.Kafka.Admin;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Xml.Serialization;
using UserService.Context;
using UserService.Dtos;
using UserService.Models;

namespace UserService.Handler
{
    public class StepHandler : BackgroundService
    {
      //  private readonly string server = "localhost:9092";
        private readonly string server = "kafka:9092";
        private readonly string topic = "user_creations";
        private static UserContext _context;
        private static IConfiguration _configuration;


        public StepHandler(IConfiguration config)
        {
            _configuration = config;
        }

        private static void InitializeContex()
        {
            _context = new UserContext();
        }

        private void CreateTopic()
        {
            using (var adminClient = new AdminClientBuilder(new AdminClientConfig { BootstrapServers = server }).Build())
            {
                try
                {
                    adminClient.CreateTopicsAsync
                        (
                        new TopicSpecification[] {

                           new TopicSpecification { Name = topic, ReplicationFactor = 1, NumPartitions = 1 },
            });
                }
                catch (CreateTopicsException e)
                {
                    Console.WriteLine($"An error occured creating topic {e.Results[0].Topic}: {e.Results[0].Error.Reason}");
                }
            }
        }


        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {

            InitializeContex();
            CreateTopic();
            var config = new ConsumerConfig()
            {
                GroupId = "st_consumer_group",
                BootstrapServers = server,
                AutoOffsetReset = AutoOffsetReset.Earliest,
                AllowAutoCreateTopics = true
            };


            await Task.Run(() =>
            {
                using (var consumer = new ConsumerBuilder<Ignore, string>(config).Build())
                {
                    config.AllowAutoCreateTopics = true;
                    consumer.Subscribe(topic);

                    while (!stoppingToken.IsCancellationRequested)
                    {
                        var consumeResult = consumer.Consume(stoppingToken);
                        // var consumer = builder.Consume(cancelToken.Token);
                        var serializer = new XmlSerializer(typeof(KafkaUserDto));
                        KafkaUserDto userDto;
                        using (TextReader reader = new StringReader(consumeResult.Message.Value))
                        {
                            userDto = (KafkaUserDto)serializer.Deserialize(reader);
                        }
                        User user = new User()
                        {
                            Id = userDto.Id,
                            email = userDto.Email,
                            username = userDto.Username
                        };
                        _context.addUser(user);

                        //   var test = consumer.Message.Value;
                        //  System.Diagnostics.Debug.WriteLine($"Message: {consumer.Message.Value} received from {consumer.TopicPartitionOffset}");
                    }
                    consumer.Close();
                }
            });
        }






    }
}

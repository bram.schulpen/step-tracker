﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Net.Http.Headers;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Threading.Tasks;
using UserService.Context;
using UserService.Dtos;
using UserService.Handler;
using UserService.Logic;
using UserService.Models;

namespace UserService.Controllers
{
    //fix error handling
    [Route("api/v1")]
    [ApiController]
    public class UserController : ControllerBase
    {
        private readonly UserLogic _logic;

        public UserController() {
            UserContext context = new UserContext();
            UserLogic logic = new UserLogic(context);
            _logic = logic;
        }





        [HttpGet]
        [Route("/users/getAchievements")]
        public async Task<IActionResult> GetAchievements()
        {
            try
            {
                string accessToken = Request.Headers[HeaderNames.Authorization].ToString().Replace("Bearer ", string.Empty);
                string userId = getSenderId(accessToken);
                List<Achievement> response = await _logic.GetAchievement(userId);
                return new JsonResult(response);
            }
            catch (UnauthorizedAccessException)
            {
                return new UnauthorizedResult();
            }
            catch (Exception e)
            {
                return new BadRequestResult();
            }
        }

        [HttpGet]
        [Route("/users/getUsers")]
        public IActionResult getAllUsers()
        {
            try
            {
                string accessToken = Request.Headers[HeaderNames.Authorization].ToString().Replace("Bearer ", string.Empty);
                string userId = getSenderId(accessToken);
                List<UserDto> response = _logic.getAllUsers(Guid.Parse(userId)); // stop sending all users....
                return new JsonResult(response);
            }
            catch (UnauthorizedAccessException)
            {
                return new UnauthorizedResult();
            }
            catch (Exception e)
            {
                return new BadRequestResult();
            }
        }

        [HttpGet]
        [Route("/users/getUser/{username}")]
        public IActionResult getUser(string username)
        {
            try
            {
                string accessToken = Request.Headers[HeaderNames.Authorization].ToString().Replace("Bearer ", string.Empty);
                string userId = getSenderId(accessToken);
                UserDto response = _logic.getUser(Guid.Parse(userId), username);
                return new JsonResult(response);
            }
            catch (UnauthorizedAccessException)
            {
                return new UnauthorizedResult();
            }
            catch (Exception e)
            {
                return new BadRequestResult();
            }
        }

        [HttpGet]
        [Route("/follow/getFollowing")]
        public IActionResult getFollowing()
        {
            try
            {
                string accessToken = Request.Headers[HeaderNames.Authorization].ToString().Replace("Bearer ", string.Empty);
                string userId = getSenderId(accessToken);
                List<Followed> response = _logic.getFollowed(Guid.Parse(userId));
                return new JsonResult(response);
            }
            catch (UnauthorizedAccessException) 
            {
                return new UnauthorizedResult();
            }
            catch (Exception)
            {
                return new BadRequestResult();
            }
        }


        [HttpGet]
        [Route("/userSteps")]
        public async Task<IActionResult> GetPersonalSteps()
        {
            try
            {
                string accessToken = Request.Headers[HeaderNames.Authorization].ToString().Replace("Bearer ", string.Empty);
                string userId = getSenderId(accessToken);
                List<FollowedStepsDto> response = await _logic.GetUserSteps(userId);

                return new JsonResult(response);
            }
            catch (UnauthorizedAccessException)
            {
                return new UnauthorizedResult();
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return new JsonResult(e);
            }
        }


        [HttpGet]
        [Route("/steps")]
        public async Task<IActionResult> getStepsAsync()
        {
            try
            {
                string accessToken = Request.Headers[HeaderNames.Authorization].ToString().Replace("Bearer ", string.Empty);
                string userId = getSenderId(accessToken);
                List<FollowedStepsDto> response = await _logic.GetFollowedStepsAsync(userId);
                       
                return new JsonResult(response);
            }
            catch (UnauthorizedAccessException)
            {
                return new UnauthorizedResult();
            }
            catch (Exception)
            {
                return new BadRequestResult();
            }
        }
        
        
        [HttpPost]
        [Route("/follow/{followingId}")]
        public IActionResult followUser(Guid followingId)
        {
            try
            {

                string accessToken = Request.Headers[HeaderNames.Authorization].ToString().Replace("Bearer ", string.Empty);
                string userId = getSenderId(accessToken);
                _logic.followUser(Guid.Parse(userId), followingId);
                return Ok();
            }
            catch (Exception)
            {
                return new BadRequestResult();
            }
        }

        [HttpPost]
        [Route("/unfollow/{followingId}")]
        public IActionResult unFollowUser(Guid followingId)
        {
            try
            {
                string accessToken = Request.Headers[HeaderNames.Authorization].ToString().Replace("Bearer ", string.Empty);
                string userId = getSenderId(accessToken);
                _logic.unfollowUser(Guid.Parse(userId), followingId);
                return Ok();
            }

            catch (Exception)
            {
                return new BadRequestResult();
            }
        }


        private string getSenderId(string accessToken) {
            var handler = new JwtSecurityTokenHandler();
            var jsonToken = handler.ReadJwtToken(accessToken);
            string userId = jsonToken.Claims.First(i => i.Type == "sid").Value;

            return userId;
        }
    }
}

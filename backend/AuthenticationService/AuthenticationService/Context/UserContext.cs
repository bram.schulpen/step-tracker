﻿using System;
using MongoDB.Driver;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AuthenticationService.Models;
using MongoDB.Bson;

namespace AuthenticationService.Context
{
    public class UserContext
    {

        private IMongoDatabase db;

        public UserContext(string database = "LoginDatabase") {

            MongoClient dbClient = new MongoClient("mongodb+srv://bram:fIknVRs3yPsiZDmq@cluster0mongo.ftdqk.mongodb.net/LoginDatabase?retryWrites=true&w=majority");
            db = dbClient.GetDatabase(database);
        }


        public bool UserNameExists(string username) {
            try
            {
                string table = "Users";
                User user = LoadUserByUsername<User>(table, username);
                return true;
            }

            catch(Exception e) {
                return false;
            }

        }

        public void RegisterUser<T>(User user) {
            InsertRecord("Users", user);

        }



        // this should be in a parent class
        private void InsertRecord<T>(string table, T record) {
            IMongoCollection<T> collection = db.GetCollection<T>(table);
            collection.InsertOne(record);
        }


        // for smaller to moderate sides lists, it is faster to first load all records and then filter
        private List<T> LoadRecords<T>(string table) {
            IMongoCollection<T> collection = db.GetCollection<T>(table);
            return collection.Find(new BsonDocument()).ToList();
        }

        public T LoadRecordById<T>(string table, Guid id) {
            IMongoCollection<T> collection = db.GetCollection<T>(table);
            FilterDefinition<T> filter = Builders<T>.Filter.Eq("id",id);
            return collection.Find(filter).First();
        }

        public T LoadUserByUsername<T>(string table, string username)
        {
                IMongoCollection<T> collection = db.GetCollection<T>(table);
                FilterDefinition<T> filter = Builders<T>.Filter.Eq("Username", username);
                return collection.Find(filter).First();
            }
    }
}

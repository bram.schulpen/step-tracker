﻿using AuthenticationService.Context;
using AuthenticationService.Logic;
using AuthenticationService.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Data;
using System.IdentityModel.Tokens.Jwt;
using System.Text;
using System.Security.Claims;
using AuthenticationService.Dto;
using Prometheus;
using System.Diagnostics;
using System.Net;
using System.Linq;
using Serilog;
using Serilog.Sinks.Loki;
using Confluent.Kafka;
using Confluent.Kafka.Admin;

namespace AuthenticationService.Controllers
{

    [ApiController]
    [Route("[controller]")]
    public class LoginController : ControllerBase
    {
        private Counter CallNumber = Metrics.CreateCounter("login_service_requests_total", "The total number of calls to the login server");

        private IConfiguration _config;

        private readonly LoginLogic logic;
        public LoginController(IConfiguration config)
        {
            _config = config;
            UserContext userContext = new UserContext();
            logic = new LoginLogic(userContext);
        }

        [HttpPost]
        [Route("/Login")]
        [AllowAnonymous]
        public IActionResult LoginToSystem([FromBody] UserDto userDto)
        {
            CallNumber.Inc();
        //    IPAddress remoteIpAddress = Request.HttpContext.Connection.RemoteIpAddress;
            string ipAdress = getIp();
            try
            {
                IActionResult response = Unauthorized();
                User found_user = logic.LoginUser(userDto.Username, userDto.Password, ipAdress);

                if (found_user != null)
                {
                    var tokenString = GenerateJSONWebToken(found_user);
                    response = Ok(tokenString);
                }

                return response;
            }
            catch (Exception e)
            {
                return NoContent();
            }
        }



        [HttpPost]
        [Route("/Register")]
        [AllowAnonymous]
        public IActionResult RegisterUser([FromBody] UserDto userDto)
        {
            CallNumber.Inc();
            try
            {
                logic.RegisterUser(userDto.Username, userDto.Password, userDto.Email);
                return new OkResult();
            }
            catch (DuplicateNameException)
            {
                return new BadRequestResult();
            }
            catch (Exception)
            {
                return new BadRequestResult();
            }
        }

        [HttpDelete]
        [Route("/RemoveAccount")]
        [Authorize]
        public IActionResult RemoveAccount()
        {
            string ipAdress = getIp();
            int importance_level = 3;
            string description = "Removing user account";
            Guid id = Guid.NewGuid();
            var credentials = new NoAuthCredentials("http://192.168.43.130:3100"); // Address to local or remote Loki server
            string type = "Remove Account";
            Log.Logger = new LoggerConfiguration()
                    .MinimumLevel.Information()
                    .Enrich.FromLogContext()
                    .WriteTo.LokiHttp(credentials)
                    .CreateLogger();
            Log.Information("id: {@id}, ip:{@ip}, Event_type: {@type}, importance_level: {@importance_level}, description:{@description}", id, ipAdress, type, importance_level, description);
            Log.CloseAndFlush();
            return Ok();
        }


        private string GenerateJSONWebToken(User user)
        {
            var securityKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_config["Jwt:Key"]));
            var credentials = new SigningCredentials(securityKey, SecurityAlgorithms.HmacSha256);

            var claims = new[] {
                new Claim(JwtRegisteredClaimNames.Sid, user.Id.ToString()),
                new Claim(JwtRegisteredClaimNames.Email, user.Email),
                new Claim("Username", user.Username),
            };

            var token = new JwtSecurityToken(_config["Jwt:Issuer"],
                _config["Jwt:Issuer"],
                claims,
                expires: DateTime.Now.AddDays(1),
                signingCredentials: credentials);

            return new JwtSecurityTokenHandler().WriteToken(token);
        }

        private string getIp()
        {
            IPAddress remoteIpAddress = Request.HttpContext.Connection.RemoteIpAddress;
            string result = "";
            if (remoteIpAddress != null)
            {
                
                if (remoteIpAddress.AddressFamily == System.Net.Sockets.AddressFamily.InterNetworkV6)
                {
                    remoteIpAddress = System.Net.Dns.GetHostEntry(remoteIpAddress).AddressList
            .First(x => x.AddressFamily == System.Net.Sockets.AddressFamily.InterNetwork);
                }
                result = remoteIpAddress.ToString();
                
            }
            return result;
        }
        //                //result = "192.168.43.130";
    }
}

﻿using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Threading.Tasks;

namespace AuthenticationService.Models
{
    public class User
    {

        [BsonId]
        public Guid Id { get; set; }

        [BsonRequired]
        public string Username { get; set; }

        [BsonRequired]
        public string Email { get; set; }

        [BsonRequired]
        public string Password { get; set; }

        public string Salt { get; set; }

    }
}

﻿using AuthenticationService.Dto;
using AuthenticationService.Models;
using Confluent.Kafka;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace AuthenticationService.Logic
{
    public class CreationMessageProducer
    {
        private readonly ProducerConfig producerConfig = new ProducerConfig
        {
           // BootstrapServers = "localhost:9092"
            BootstrapServers = "kafka:9092"
        };



        public void AddUser(User user)
        {
            using (var producer = new ProducerBuilder<Null, string>(producerConfig).Build())
            {
                string topic = "user_creations";
                try
                {
                    string message = SerializeStep(user);
                    var deliveryResult = producer.ProduceAsync(topic, new Message<Null, string> { Value = message })
                        .GetAwaiter()
                        .GetResult();
                }
                catch (Exception e)
                {
                    Console.WriteLine($"Oops, something went wrong: {e}");
                }
            }
        }

        // this can't be a good way to send steps :/
        private string SerializeStep(User user)
        {
            KafkaUserDto kafkaUserDto = new KafkaUserDto
            {
                Id = user.Id,
                Username = user.Username,
                Email = user.Email
            };

            XmlSerializer xmlSerializer = new XmlSerializer(kafkaUserDto.GetType());

            using (StringWriter textWriter = new StringWriter())
            {
                xmlSerializer.Serialize(textWriter, kafkaUserDto);
                return textWriter.ToString();
            }
        }
    }
}

﻿using AuthenticationService.Context;
using AuthenticationService.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Threading.Tasks;
using System.Text;
using System.Data;
using Microsoft.AspNetCore.Mvc;
using System.Text.RegularExpressions;
using System.Net.Mail;
using Prometheus;
using System.Diagnostics;
using Serilog.Sinks.Loki;
using Serilog;

namespace AuthenticationService.Logic
{
    public class LoginLogic
    {
        private CreationMessageProducer creationMessageProducer;
        private int salthLength = 32;
      
        private Counter TotalLoginFailCount = Metrics.CreateCounter("login_service_login_fail", "The total number of calls to the login server");
        private Counter TotalLoginSuccessCount = Metrics.CreateCounter("login_service_login_success", "The total number of calls to the login server");


        private readonly UserContext context;
        public LoginLogic(UserContext userContext)
        {
            context = userContext;
            creationMessageProducer = new CreationMessageProducer();
        }

        private void AddFailedLogin(string ip) {
            int importance_level = 1;
            string description = "Login failed";
            Guid id = Guid.NewGuid();
            var credentials = new NoAuthCredentials("http://192.168.43.130:3100"); // Address to local or remote Loki server
            string type = "failed login";
            Log.Logger = new LoggerConfiguration()
                    .MinimumLevel.Information()
                    .Enrich.FromLogContext()
                    .WriteTo.LokiHttp(credentials)
                    .CreateLogger();
            Log.Information("id{@id}, ip:{@ip}, Event_type: {@type}, importance_level: {@importance_level}, description:{@description}", id, ip, type, importance_level, description);

            Log.CloseAndFlush();
        }


        public User LoginUser(string username, string password, string ipAdress) {
            try
            {
                User user = context.LoadUserByUsername<User>("Users", username);
                byte[] user_salt = Convert.FromBase64String(user.Salt);
                byte[] hashedPassword = GetHash(password);
                byte[] encrypedPasswordByte = GetEncryptedPassword(hashedPassword, user_salt);
                string encrypedPassword = Convert.ToBase64String(encrypedPasswordByte);
                if (encrypedPassword == user.Password)
                {
                    TotalLoginSuccessCount.Inc();
                    return user;
                }

                else
                {
                    TotalLoginFailCount.Inc();
                    AddFailedLogin(ipAdress);
                    return null;
                }
            }
            catch {
            return null;
            }
        }


        private byte[] GetHash(string inputString)
        {
            using (HashAlgorithm algorithm = SHA256.Create())
                return algorithm.ComputeHash(Encoding.UTF8.GetBytes(inputString));
        }

        //Used RNGCryptoServiceProvider given security standerd with random number generator
        private byte[] GetSalt() {
            byte[] salt = new byte[salthLength];
            using (var random = new RNGCryptoServiceProvider())
            {
                random.GetNonZeroBytes(salt);
            }
            return salt;
        }

        private byte[]  GetEncryptedPassword(byte[] password, byte[] salt) {
            HashAlgorithm algorithm = new SHA256Managed();

            byte[] plainTextWithSaltBytes =
              new byte[password.Length + salt.Length];

            for (int i = 0; i < password.Length; i++)
            {
                plainTextWithSaltBytes[i] = password[i];
            }
            for (int i = 0; i < salt.Length; i++)
            {
                plainTextWithSaltBytes[password.Length + i] = salt[i];
            }

            byte[] encryptedPassword = algorithm.ComputeHash(plainTextWithSaltBytes);
            
            return encryptedPassword;
        }

        
        public User RegisterUser(string username, string password, string email) {
            byte[] byteSalt = GetSalt();
            byte[] hashedPassword = GetHash(password);
            byte[] encrypedPasswordByte = GetEncryptedPassword(hashedPassword,byteSalt);
            string encrypedPassword = Convert.ToBase64String(encrypedPasswordByte);
            string salt = Convert.ToBase64String(byteSalt);
            if (!context.UserNameExists(username) && StrongCredentials(password,username,email))
            {
                User user = new User { Username = username, Password = encrypedPassword, Salt = salt, Email = email };
                context.RegisterUser<User>(user);
                creationMessageProducer.AddUser(user);
                return user;
            }
            else {
                throw new ArgumentException();
            }
        }

        //I had to make the StrongCredentials boolean Public for testing reasons
        public bool StrongCredentials(string password,string username, string email) {
            Regex validEmail = new Regex(@"^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$");
            Regex hasNumber = new Regex(@"[0-9]+");
            Regex hasUpperChar = new Regex(@"[A-Z]+");
            Regex hasMinimum8Chars = new Regex(@".{8,}");
            bool isValidated = hasNumber.IsMatch(password) && hasUpperChar.IsMatch(password) && hasMinimum8Chars.IsMatch(password) && hasMinimum8Chars.IsMatch(username) && validEmail.IsMatch(email);
            return isValidated;
        }
    }
}

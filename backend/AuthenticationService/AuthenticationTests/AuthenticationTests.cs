using AuthenticationService.Context;
using AuthenticationService.Logic;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace AuthenticationTests
{
    [TestClass]
    public class AuthenticationTests
    {
        UserContext _context; 

        [TestInitialize()]
        public void TestInitialize()
        {
            _context = new UserContext();         
        }


        [TestMethod]
        public void ValidatePasswordInvalid()
        {
            //arrange 
            LoginLogic loginLogic = new LoginLogic(_context);
            string password = "weak";
            string email = "test.w@test.nl";
            string username = "Test12345!";

            //act
            bool result = loginLogic.StrongCredentials(password,username,email);

            //assert;
            Assert.IsFalse(result);
        }


        [TestMethod]
        public void ValidateEmailInvalid()
        {
            //arrange 
            LoginLogic loginLogic = new LoginLogic(_context);
            string password = "Strong12345!";
            string email = "test.test.nl";
            string username = "Test12345!";

            //act
            bool result = loginLogic.StrongCredentials(password, username, email);

            //assert;
            Assert.IsFalse(result);
        }


        [TestMethod]
        public void ValidateUsernameInvalid()
        {
            //arrange 
            LoginLogic loginLogic = new LoginLogic(_context);
            string password = "Strong12345!";
            string email = "test.w@test.nl";
            string username = "short";

            //act
            bool result = loginLogic.StrongCredentials(password, username, email);

            //assert;
            Assert.IsFalse(result);
        }

        [TestMethod]
        public void ValidateAllCredentialsValid()
        {
            //arrange 
            LoginLogic loginLogic = new LoginLogic(_context);
            string password = "Strong12345!";
            string email = "test.w@test.nl";
            string username = "Test12345!";

            //act
            bool result = loginLogic.StrongCredentials(password, username, email);

            //assert;
            Assert.IsTrue(result);
        }

    }
}

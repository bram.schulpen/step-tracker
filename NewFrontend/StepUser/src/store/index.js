import { createStore } from "vuex";
import router from "../router";

export default createStore({
  state: {
    loggedIn: localStorage.getItem('token')!= null
  },
  mutations: {
      setLoggedIn(state){
        state.loggedIn = true
        console.log('changing state...')
      }
      


    //methods that change data in the states
  },
  actions: {
    log_out(){
      console.log('changing current state...');
      localStorage.clear();
      this.state.loggedIn =false;

      router.push('Home');
    }
    //
  },
  getters:{
    get_loggedIn(state){
      return state.loggedIn
    }
    // can give data in state but change the data before giving it to the user
  },
  modules: {
  }
})

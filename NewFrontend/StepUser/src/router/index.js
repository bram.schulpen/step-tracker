import { createRouter, createWebHistory } from "vue-router";
import Login from "../views/Login.vue";
import Register from "../views/Register.vue";
import Dashboard from "../views/Dashboard.vue";
import Friends from "../views/Followers.vue";
import Home from "../views/Home.vue";
import FAQ from "../views/FAQ.vue";
import Privacy from "../views/Privacy.vue";


const routes = [
  {
    path: "/",
    name: "Home",
    component: Home,
    meta: {
      guest: true
    }
  },
  {
    path: "/Home",
    name: "Home",
    component: Home,
    meta: {
      guest: true
    }
  },
  {
    path: "/FAQ",
    name: "FAQ",
    component: FAQ,
    meta: {
      guest: true
    }
  },
  {
    path: "/Privacy",
    name: "Privacy",
    component: Privacy,
    meta: {
      guest: true
    }
  },
  {
    path: "/Login",
    name: "Login",
    component: Login,
    meta: {
      guest: true
    }
  },
  {
    path: "/Register",
    name: "Register",
    component: Register,
    meta: {
      guest: true
    }
  },
  {
    path: "/Dashboard",
    name: "Dashboard",
    component: Dashboard,
    meta: {
        requiresAuth: true
    }
  },
  
  {
    path: "/Friends",
    name: "Friends",
    component: Friends,
    meta: {
        requiresAuth: true
    }
}
];
const router = createRouter({
  history: createWebHistory(),
  routes,
});


//  //Checks if auth is required, if so it checks if there is a Authtoken
router.beforeEach((to, from, next) => {
  if(to.matched.some(record => record.meta.requiresAuth)) {
    if (localStorage.token != null) {
      next()
      return
    }
    next('/Login')
  } else {
    next()
  }
})

export default router;
